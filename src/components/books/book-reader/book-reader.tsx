import { Button } from '@mui/material';
import { UserPreview } from '../../../types/user/user.preview';
import { InfoCard } from '../../shared/info-card/info-card';

export const BookReader = (props: BookReaderProps) => {
  const { user } = props;

  const handleEndBorrowClick = () => props.onEndBorrow(user);

  return (
    <InfoCard title={user.firstName + ' ' + user.lastName} subheader={user.id}>
      <Button variant="contained" onClick={handleEndBorrowClick}>
                סיים השאלה
      </Button>
    </InfoCard>
  );
};

interface BookReaderProps {
    user: UserPreview;
    onEndBorrow: (user: UserPreview) => void;
}
