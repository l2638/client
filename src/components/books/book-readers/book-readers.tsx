import { ListItem } from '@mui/material';
import { Box } from '@mui/system';
import { FC } from 'react';
import { toast } from 'react-toastify';
import { useAddBorrowMutation, useEndBorrowMutation } from '../../../apis/borrow.api';
import { useGetUsersByBookQuery } from '../../../apis/user.api';
import { useDialog } from '../../../hooks/use-dialog';
import { Book } from '../../../types/book/book';
import { AddBorrowFormData as Borrow } from '../../../types/borrow/add-borrow-form-data';
import { UserPreview } from '../../../types/user/user.preview';
import { fromTrigger } from '../../../utils/notifications/from-trigger';
import { AddBorrowSchema } from '../../../utils/validation-schemas/borrow/add-borrow.schema';
import { AddBorrowForm } from '../../borrow/add-borrow-form/add-borrow-form';
import { FormikDialog } from '../../shared/formik-dialog/formik-dialog';
import { FormikExtended } from '../../shared/formik-extended/formik-extended';
import { ListHeader } from '../../shared/list-header/list-header';
import { ScrollableList } from '../../shared/scrollable-list/scrollable-list';
import { BookReader } from '../book-reader/book-reader';

export const BookReaders: FC<Props> = ({ book }) => {
    const { data: users, isFetching } = useGetUsersByBookQuery(book.id);

    const [endBorrow] = useEndBorrowMutation();
    const [addBorrow] = useAddBorrowMutation();

    const { openDialog, closeDialog } = useDialog();

    const openBorrowDialog = () => {
        openDialog(
            <FormikExtended
                initialValues={{ book, user: null } as unknown as Borrow}
                onSubmit={handleAddBorrow}
                validationSchema={AddBorrowSchema}
                disabled={{ book: {} }}
            >
                <FormikDialog
                    title='השאלת ספר'
                >
                    <AddBorrowForm />
                </FormikDialog>
            </FormikExtended>
        );
    };

    const handleEndBorrow = (user: UserPreview) => {
        fromTrigger(endBorrow({
            userId: user.id,
            bookId: book.id,
        }));
    };

    const handleAddBorrow = ({ user }: Borrow): void => {
        if (users?.find((u) => u.id === user.id)) {
            toast.info(`ספר זה כבר מושאל ע"י המשתמש הנבחר`);
        } else {
            fromTrigger(
                addBorrow({
                    userId: user.id,
                    bookId: book.id,
                }))
                .then(closeDialog);
        }
    };

    const hasNoAvailableCopies = users?.length === book.copiesCount;

    return (
        <Box display="flex" flexDirection="column" height='inherit'>
            <ListHeader
                title={`הקוראים של הספר: ${book.title}`}
                actionName="השאל ספר" onClick={openBorrowDialog}
                disabled={isFetching || hasNoAvailableCopies}
                tooltip={hasNoAvailableCopies ? 'כל העותקים כרגע מושאלים' : ''}
            />
            <ScrollableList>
                {users?.map((user) => (
                    <ListItem key={user.id}>
                        <BookReader user={user} onEndBorrow={handleEndBorrow} />
                    </ListItem>
                ))}
            </ScrollableList>
        </Box>
    );
};

interface Props {
    book: Book;
}