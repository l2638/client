import { Book } from '../../../types/book/book';
import { EditDeleteCard } from '../../shared/edit-delete-card-info/edit-delete-card-info';

export const BookCard = (props: BookCardProps) => {
  const { book } = props;

  const handleClick = (): void => props.onClick(book);

  const handleEdit = (): void => props.onEdit(book);

  return (
    <EditDeleteCard
      title={book.title}
      subheader={`עותקים: ${book.copiesCount} סופר: ${book.author.firstName} ${book.author.lastName}`}
      selected={props.selected}
      onClick={handleClick}
      onEdit={handleEdit}
    />
  );
};

interface BookCardProps {
    book: Book;
    onClick: (book: Book) => void;
    onEdit: (book: Book) => void;
    selected?: boolean;
}
