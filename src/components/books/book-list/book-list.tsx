import { Box, ListItem } from '@mui/material';
import { Formik, FormikHelpers } from 'formik';
import { FC } from 'react';
import { useAddBookMutation, useGetBooksQuery, useUpdateBookMutation } from '../../../apis/book.api';
import { useDialog } from '../../../hooks/use-dialog';
import { Book } from '../../../types/book/book';
import { fromTrigger } from '../../../utils/notifications/from-trigger';
import { AddBookSchema } from '../../../utils/validation-schemas/book/add-book.schema';
import { getEditBookSchema } from '../../../utils/validation-schemas/book/edit-book.schema';
import { FormikDialog } from '../../shared/formik-dialog/formik-dialog';
import { FormikExtended } from '../../shared/formik-extended/formik-extended';
import { ListHeader } from '../../shared/list-header/list-header';
import { ScrollableList } from '../../shared/scrollable-list/scrollable-list';
import { BookCard } from '../book-card/book-card';
import { BookSaveForm } from '../book-save-form/book-save-form';

export const BookList: FC<Props> = ({ onClick, selectedBook }) => {
    const ADD_BOOK_INITIAL_VALUES: Book = {
        id: 0,
        title: '',
        copiesCount: 0,
        author: {
            id: 0,
            firstName: '',
            lastName: '',
        },
    };

    const { data: books } = useGetBooksQuery();

    const [addBook] = useAddBookMutation();
    const [updateBook] = useUpdateBookMutation();

    const { openDialog, closeDialog } = useDialog();

    const handleClick = (book: Book): void => {
        closeDialog();
        onClick(book);
    };

    const showEditDialog = (book: Book): void => {
        openDialog(
            <FormikExtended
                initialValues={book}
                onSubmit={handleEditBook}
                validationSchema={getEditBookSchema(book)}
                disabled={{ title: true }}
            >
                <FormikDialog title='עריכת סופר' >
                    <BookSaveForm />
                </FormikDialog>
            </FormikExtended>,
        );
    };

    const showAddDialog = (): void => {
        openDialog(
            <Formik
                initialValues={ADD_BOOK_INITIAL_VALUES}
                onSubmit={handleAddBook}
                validationSchema={AddBookSchema}
            >
                <FormikDialog title='הוספת ספר'>
                    <BookSaveForm />
                </FormikDialog>
            </Formik>,
        );
    };

    const handleAddBook = (book: Book, { setSubmitting }: FormikHelpers<Book>): void => {
        fromTrigger(addBook(book))
            .then(handleClick)
            .finally(() => setSubmitting(false));
    };

    const handleEditBook = (book: Book, { setSubmitting }: FormikHelpers<Book>): void => {
        fromTrigger(updateBook({
            id: book.id,
            book,
        }))
            .then(handleClick)
            .finally(() => setSubmitting(false));
    };

    return (
        <Box display="flex" flexDirection="column" height='inherit'>
            <ListHeader title="רשימת הספרים" actionName="הוסף ספר" onClick={showAddDialog} />
            <ScrollableList>
                {books?.map((book) => (
                    <ListItem key={book.id}>
                        <BookCard
                            book={book}
                            selected={book.id === selectedBook?.id}
                            onClick={handleClick}
                            onEdit={showEditDialog}
                        />
                    </ListItem>
                ))}
            </ScrollableList>
        </Box>
    );
};

interface Props {
    onClick: (book: Book) => void;
    selectedBook: Book | null;
}
