import { Autocomplete, Button, CircularProgress, Grid, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { FC, SyntheticEvent, useRef, useState } from 'react';
import { useGetAuthorsPreviewsQuery } from '../../../apis/author.api';
import { useFormikContextExtended } from '../../../hooks/use-formik-context-extended';
import { Author } from '../../../types/author/author';
import { BookCreate } from '../../../types/book/book.create';

export const BookSaveForm: FC = () => {
  const [isExtendedViewOpen, setExtendedViewOpen] = useState(false);

  const {
    handleChange,
    setFieldValue,
    values,
    errors,
    touched,
    disabled,
  } = useFormikContextExtended<BookCreate>();

  const { data: authors = [], isLoading } = useGetAuthorsPreviewsQuery(undefined, { skip: !!disabled.author });

  const authorInputRef = useRef<HTMLInputElement>();

  const handleAuthorChange = (_event: SyntheticEvent, author: Author) => {
    setExtendedViewOpen(false);
    setFieldValue('author', author);
  };

  const showExtendedView = (): void => {
    if (authorInputRef.current) {
      const [firstName, lastName] = authorInputRef.current.value.split(/ (.*)/s);

      setFieldValue('author', {
        id: null,
        firstName,
        lastName: lastName || '',
      });

      authorInputRef.current.blur();
    }

    setExtendedViewOpen(true);
  };

  return (
    <Grid container direction="column" rowSpacing={1}>
      <Grid item>
        <TextField
          id="title"
          name="title"
          label='שם ספר'
          value={values.title}
          onChange={handleChange}
          error={touched.title && !!errors.title}
          helperText={touched.title && errors.title || ' '}
          disabled={disabled.title}
        />
      </Grid>
      <Grid item>
        <TextField
          id="copiesCount"
          name="copiesCount"
          label='כמות עותקים'
          type='number'
          value={values.copiesCount}
          onChange={handleChange}
          error={touched.copiesCount && !!errors.copiesCount}
          helperText={touched.copiesCount && errors.copiesCount || ' '}
          disabled={disabled.copiesCount}
        />
      </Grid>
      <Grid item>
        <Autocomplete
          options={authors}
          getOptionLabel={(option) => option.firstName + ' ' + option.lastName}
          isOptionEqualToValue={(option, value) => option.id === value.id}
          value={values.author}
          disabled={!!disabled.author || isLoading}
          sx={{ width: 300 }}
          ListboxProps={{ style: { maxHeight: 200, overflow: 'auto' } }}
          onChange={handleAuthorChange}
          disableClearable
          renderInput={(params) => <TextField
            inputRef={authorInputRef}
            name="author"
            value={values.author}
            {...params}
            label="סופר/ת"
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <>
                  {isLoading && <CircularProgress color="inherit" size={20} />}
                  {params.InputProps.endAdornment}
                </>
              ),
            }}
            error={touched.author && !!errors.author}
            helperText={(!!touched.author && errors.author && 'שם סופר הוא חובה') || ' '}
          />}
          noOptionsText={
            <Box display="flex" justifyContent="space-between" alignItems="center">
              סופר לא קיים
              <Button
                variant="outlined"
                onClick={showExtendedView}
              >
                הקלד ידנית
              </Button>
            </Box>
          }
          renderOption={(props, option) => (
            <li {...props} key={option.id}>
              {option.firstName + ' ' + option.lastName}
            </li>
          )} />

        {isExtendedViewOpen &&
          <Grid item container spacing={2}>
            <Grid item>
              <TextField
                id="firstName"
                name="author.firstName"
                label='שם פרטי'
                value={values.author?.firstName}
                disabled={disabled.author?.firstName}
                onChange={handleChange}
                error={touched.author?.firstName && !!errors.author?.firstName}
                helperText={touched.author?.firstName && errors.author?.firstName || ' '}
              />
            </Grid>
            <Grid item>
              <TextField
                id="lastName"
                name="author.lastName"
                label='שם משפחה'
                value={values.author?.lastName}
                disabled={disabled.author?.lastName}
                onChange={handleChange}
                error={touched.author?.lastName && !!errors.author?.lastName}
                helperText={touched.author?.lastName && errors.author?.lastName || ' '}
              />
            </Grid>
          </Grid>
        }
      </Grid>
    </Grid>
  );
};
