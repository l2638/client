import { FC } from 'react';
import { User } from '../../../types/user/user';
import { AreYouSureDialog } from '../../shared/are-you-sure-dialog/are-you-sure-dialog';

export const UserDeleteDialog: FC<Props> = ({ user, onApprove, isSubmitting }) => {
  return (
    <AreYouSureDialog
      title={`האם הינך בטוח שברצונך למחוק את המשתמש/ת ${user.firstName} ${user.lastName}?`}
      content="מחיקת המשתמש תסיים את כל ההשאלות שלו"
      onApprove={() => onApprove(user)}
      isSubmitting={isSubmitting}
    />
  );
};

interface Props {
    user: User;
    onApprove: (user: User) => void;
    isSubmitting: boolean;
}
