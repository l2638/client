import { Box, ListItem } from '@mui/material';
import { Formik, FormikHelpers } from 'formik';
import { some } from 'lodash';
import { FC } from 'react';
import { toast } from 'react-toastify';
import { useAddUserMutation, useDeleteUserMutation, useGetUsersQuery, useUpdateUserMutation } from '../../../apis/user.api';
import { useDialog } from '../../../hooks/use-dialog';
import { User } from '../../../types/user/user';
import { UserCreate } from '../../../types/user/user.create';
import { fromTrigger } from '../../../utils/notifications/from-trigger';
import { UserSchema } from '../../../utils/validation-schemas/user/user.schema';
import { FormikDialog } from '../../shared/formik-dialog/formik-dialog';
import { FormikExtended } from '../../shared/formik-extended/formik-extended';
import { ListHeader } from '../../shared/list-header/list-header';
import { ScrollableList } from '../../shared/scrollable-list/scrollable-list';
import { UserCard } from '../user-card/user-card';
import { UserDeleteDialog } from '../user-delete-dialog/user-delete-dialog';
import { UserSaveForm } from '../user-save-form/user-save-form';

export const UsersList: FC<Props> = ({ onClick, selectedUser }) => {
    const { data: users } = useGetUsersQuery({ sort: ["firstName", "lastName"] });
    const [deleteUser, { isLoading: isDeleting }] = useDeleteUserMutation();
    const [addUser] = useAddUserMutation();
    const [updateUser] = useUpdateUserMutation();

    const { openDialog, closeDialog } = useDialog();

    const handleUserClick = (user: User | null): void => {
        closeDialog();
        onClick(user);
    };

    const openDeleteDialog = (user: User): void => {
        openDialog(
            <UserDeleteDialog
                user={user}
                onApprove={handleDeleteUser}
                isSubmitting={isDeleting}
            />
        );
    };

    const showEditDialog = (user: User): void => {
        openDialog(
            <FormikExtended
                initialValues={user}
                validationSchema={UserSchema}
                onSubmit={handleEditUser}
                disabled={{ id: true }}
            >
                <FormikDialog title="עריכת משתמש">
                    <UserSaveForm />
                </FormikDialog>
            </FormikExtended>,
        );
    };

    const showAddDialog = (): void => {
        openDialog(
            <Formik
                initialValues={{ id: 0, firstName: '', lastName: '' }}
                validationSchema={UserSchema}
                onSubmit={handleAddUser}
            >
                <FormikDialog title='הוספת משתמש'>
                    <UserSaveForm />
                </FormikDialog>
            </Formik>
        );
    };

    const handleEditUser = (user: User, { setSubmitting }: FormikHelpers<User>) => {
        fromTrigger(updateUser({
            id: user.id,
            user: user,
        }))
            .then(handleUserClick)
            .finally(() => setSubmitting(false));
    };

    const handleAddUser = (user: UserCreate, { setSubmitting }: FormikHelpers<UserCreate>): void => {
        if (some(users, (u) => u.id === user.id)) {
            toast.info('משתמש כבר קיים');
            setSubmitting(false);
        } else {
            fromTrigger(addUser(user))
                .then(handleUserClick)
                .finally(() => setSubmitting(false));
        }
    };

    const handleDeleteUser = ({ id }: User): void => {
        fromTrigger(deleteUser(id))
            .then(() => handleUserClick(null));
    };

    return (
        <Box display="flex" flexDirection="column" height='inherit'>
            <ListHeader title="רשימת המשתמשים" actionName="הוסף משתמש" onClick={showAddDialog} />
            <ScrollableList>
                {users?.map((user) => (
                    <ListItem key={user.id}>
                        <UserCard
                            user={user}
                            selected={user.id === selectedUser?.id}
                            onClick={handleUserClick}
                            onEdit={showEditDialog}
                            onDelete={openDeleteDialog}
                        />
                    </ListItem>
                ))}
            </ScrollableList>
        </Box>
    );
};

interface Props {
    onClick: (user: User | null) => void;
    selectedUser: User | null;
}
