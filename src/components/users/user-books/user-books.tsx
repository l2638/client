import { Box, ListItem } from '@mui/material';
import { FC } from 'react';
import { toast } from 'react-toastify';
import { useGetBookByUserQuery } from '../../../apis/book.api';
import { useAddBorrowMutation, useEndBorrowMutation } from '../../../apis/borrow.api';
import {
    useAddStarMutation,
    useRemoveStarMutation
} from '../../../apis/starredBook.api';
import { useDialog } from '../../../hooks/use-dialog';
import { BookDetailed } from '../../../types/book/book.detailed';
import { AddBorrowFormData as Borrow } from '../../../types/borrow/add-borrow-form-data';
import { User } from '../../../types/user/user';
import { fromTrigger } from '../../../utils/notifications/from-trigger';
import { AddBorrowSchema } from '../../../utils/validation-schemas/borrow/add-borrow.schema';
import { AddBorrowForm } from '../../borrow/add-borrow-form/add-borrow-form';
import { FormikDialog } from '../../shared/formik-dialog/formik-dialog';
import { FormikExtended } from '../../shared/formik-extended/formik-extended';
import { ListHeader } from '../../shared/list-header/list-header';
import { ScrollableList } from '../../shared/scrollable-list/scrollable-list';
import { UserBook } from '../user-book/user-book';

export const UserBooks: FC<Props> = ({ user }) => {
    const { data: userBooks } = useGetBookByUserQuery(user.id);

    const [endBorrow] = useEndBorrowMutation();
    const [addBorrow] = useAddBorrowMutation();
    const [addStar] = useAddStarMutation();
    const [removeStar] = useRemoveStarMutation();

    const { openDialog, closeDialog } = useDialog();

    const showBorrowDialog = () => {
        openDialog(
            <FormikExtended
                initialValues={{ user, book: null } as unknown as Borrow}
                onSubmit={handleAddBorrow}
                validationSchema={AddBorrowSchema}
                disabled={{ user: {} }}
            >
                <FormikDialog
                    title='השאלת ספר'
                >
                    <AddBorrowForm />
                </FormikDialog>
            </FormikExtended>
        );
    };

    const handleEndLoan = (book: BookDetailed): void => {
        fromTrigger(endBorrow({
            bookId: book.id,
            userId: user.id,
        }));
    };

    const handleAddBorrow = ({ book }: Borrow): void => {
        if (userBooks?.find((b) => b.isActive && b.id === book.id)) {
            toast.info(`ספר זה כבר מושאל ע"י המשתמש הנבחר`);
        } else {
            fromTrigger(addBorrow({
                userId: user.id,
                bookId: book.id,
            })).then(closeDialog);
        }
    };

    const handleStarClick = (book: BookDetailed): void => {
        if (book.isStarred) {
            handleRemoveStar(book.id);
        } else {
            handleAddStar(book.id);
        }
    };

    const handleRemoveStar = (bookId: number): void => {
        fromTrigger(removeStar({
            bookId,
            userId: user.id,
        }));
    };

    const handleAddStar = (bookId: number): void => {
        fromTrigger(addStar({
            bookId,
            userId: user.id,
        }));
    };

    return (
        <Box display="flex" flexDirection="column" height='inherit'>
            <ListHeader title={`רשימת הספרים של ${user.firstName} ${user.lastName}`} actionName="השאל ספר" onClick={showBorrowDialog} />
            <ScrollableList>
                {userBooks?.map((book) => (
                    <ListItem key={book.id}>
                        <UserBook book={book} onEndBorrow={handleEndLoan} onStarClick={handleStarClick} />
                    </ListItem>
                ))}
            </ScrollableList>
        </Box>
    );
};

interface Props {
    user: User;
}