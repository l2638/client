import { Grid, TextField } from '@mui/material';
import { FC } from 'react';
import { useFormikContextExtended } from '../../../hooks/use-formik-context-extended';
import { UserCreate } from '../../../types/user/user.create';

export const UserSaveForm: FC = () => {
  const {
    values,
    errors,
    touched,
    disabled,
    handleChange,
  } = useFormikContextExtended<UserCreate>();

  return (
    <Grid container spacing={2}>
      <Grid item xs={4}>
        <TextField
          id="id"
          name="id"
          label='תעודת זהות'
          disabled={disabled.id}
          type='number'
          value={values.id}
          onChange={handleChange}
          error={touched.id && !!errors.id}
          helperText={touched.id && errors.id || ' '}
        />
      </Grid>
      <Grid item container spacing={2}>
        <Grid item>
          <TextField
            id="firstName"
            name="firstName"
            label='שם פרטי'
            disabled={disabled.firstName}
            value={values.firstName}
            onChange={handleChange}
            error={touched.firstName && !!errors.firstName}
            helperText={touched.firstName && errors.firstName || ' '}
          />
        </Grid>
        <Grid item>
          <TextField
            id="lastName"
            name="lastName"
            label='שם משפחה'
            disabled={disabled.lastName}
            value={values.lastName}
            onChange={handleChange}
            error={touched.lastName && !!errors.lastName}
            helperText={touched.lastName && errors.lastName || ' '}
          />
        </Grid>
      </Grid>
    </Grid>
  );
}
