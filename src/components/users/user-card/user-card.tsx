import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import { IconButton, Tooltip } from '@mui/material';
import { FC, MouseEvent } from 'react';
import { User } from '../../../types/user/user';
import { ClickableInfoCard } from '../../shared/clickable-info-card/clickable-info-card';

export const UserCard: FC<Props> = ({ user, onClick, onEdit, onDelete, selected }) => {
    const handleEditClick = (event: MouseEvent): void => {
        stopPropagation(event);
        onEdit(user);
    };

    const handleDeleteClick = (event: MouseEvent): void => {
        stopPropagation(event);
        onDelete(user);
    };

    const handleUserClick = () => onClick(user);

    const stopPropagation = (event: MouseEvent): void => event.stopPropagation();

    return (
        <ClickableInfoCard title={user.firstName + ' ' + user.lastName} subheader={user.id} selected={selected} onClick={handleUserClick}>
            <Tooltip title="ערוך">
                <IconButton size="large" onClick={handleEditClick} onMouseDown={stopPropagation}>
                    <EditIcon fontSize="inherit" />
                </IconButton>
            </Tooltip>
            <Tooltip title="מחק">
                <IconButton size="large" sx={{ color: '#E8223A' }} onClick={handleDeleteClick} onMouseDown={stopPropagation}>
                    <DeleteIcon fontSize="inherit" />
                </IconButton>
            </Tooltip>
        </ClickableInfoCard>
    );
};

interface Props {
    user: User;
    onClick: (user: User) => void;
    onEdit: (user: User) => void;
    onDelete: (user: User) => void;
    selected?: boolean;
}
