import StarIcon from '@mui/icons-material/Star';
import StarOutlinedIcon from '@mui/icons-material/StarOutline';
import { Button, IconButton, Tooltip } from '@mui/material';
import { BookDetailed } from '../../../types/book/book.detailed';
import { InfoCard } from '../../shared/info-card/info-card';

export const UserBook = (props: UserBookProps) => {
  const { book } = props;

  const handleEndBorrowClick = () => props.onEndBorrow(book);

  const handleStarClick = () => props.onStarClick(book);

  return (
    <InfoCard title={'שם: ' + book.title} subheader={'סופר: ' + book.author}>
      {props.book.isStarred ?
                <Tooltip title="הסר כוכב">
                  <IconButton onClick={handleStarClick}>
                    <StarIcon sx={{ color: '#FDCC0D' }} />
                  </IconButton>
                </Tooltip> :
                <Tooltip title="סימון בכוכב">
                  <IconButton onClick={handleStarClick}>
                    <StarOutlinedIcon />
                  </IconButton>
                </Tooltip>
      }
      {props.book.isActive ?
                <Button variant="contained" onClick={handleEndBorrowClick}>
                    סיים השאלה
                </Button> :
                <></>
      }
    </InfoCard>
  );
};

interface UserBookProps {
    book: BookDetailed;
    onStarClick: (book: BookDetailed) => void;
    onEndBorrow: (book: BookDetailed) => void;
}
