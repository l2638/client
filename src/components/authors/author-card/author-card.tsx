import EditIcon from '@mui/icons-material/Edit';
import { IconButton, Tooltip } from '@mui/material';
import { FC, MouseEvent } from 'react';
import { AuthorDetailed } from '../../../types/author/author.detailed';
import { ClickableInfoCard } from '../../shared/clickable-info-card/clickable-info-card';
import { DeleteButton } from '../../shared/delete-button/delete-button';

export const AuthorCard: FC<Props> = ({ author, onClick, onEdit, onDelete, selected }) => {
    const handleClick = (): void => onClick(author);

    const handleEdit = (event: MouseEvent): void => {
        stopPropagation(event);
        onEdit(author);
    };

    const handleDelete = (event: MouseEvent): void => {
        stopPropagation(event);
        onDelete(author);
    };

    const stopPropagation = (event: MouseEvent): void => event.stopPropagation();

    return (
        <ClickableInfoCard
            title={author.firstName + ' ' + author.lastName}
            selected={selected}
            onClick={handleClick}
        >
            <Tooltip title="ערוך">
                <IconButton size="large" onClick={handleEdit} onMouseDown={stopPropagation}>
                    <EditIcon fontSize="inherit" />
                </IconButton>
            </Tooltip>

            <Tooltip title={author.booksCount ? 'לא ניתן להסיר סופר עם ספרים' : 'מחק'}>
                <span>
                    <DeleteButton onClick={handleDelete} disabled={!!author.booksCount} onMouseDown={stopPropagation} />
                </span>
            </Tooltip>

        </ClickableInfoCard>
    );
};

interface Props {
    author: AuthorDetailed;
    onClick: (author: AuthorDetailed) => void;
    onEdit: (author: AuthorDetailed) => void;
    onDelete: (author: AuthorDetailed) => void;
    selected?: boolean;
}
