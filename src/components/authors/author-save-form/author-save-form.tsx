import { Grid, TextField } from '@mui/material';
import { useFormikContext } from 'formik';
import { FC } from 'react';
import { AuthorCreate } from '../../../types/author/author.create';

export const AuthorSaveForm: FC = () => {
  const {
    values,
    errors,
    touched,
    handleChange,
  } = useFormikContext<AuthorCreate>();

  return (
    <Grid container spacing={2}>
      <Grid item>
        <TextField
          id="firstName"
          name="firstName"
          label='שם פרטי'
          value={values.firstName}
          onChange={handleChange}
          error={touched.firstName && !!errors.firstName}
          helperText={touched.firstName && errors.firstName || ' '}
        />
      </Grid>
      <Grid item>
        <TextField
          id="lastName"
          name="lastName"
          label='שם משפחה'
          value={values.lastName}
          onChange={handleChange}
          error={touched.lastName && !!errors.lastName}
          helperText={touched.lastName && errors.lastName || ' '}
        />
      </Grid>
    </Grid>
  );
}
