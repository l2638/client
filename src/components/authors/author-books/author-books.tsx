import { ListItem } from '@mui/material';
import { Box } from '@mui/system';
import { FormikHelpers } from 'formik';
import { FC } from 'react';
import { useAddBookMutation, useGetBookByAuthorQuery } from '../../../apis/book.api';
import { useDialog } from '../../../hooks/use-dialog';
import { Author } from '../../../types/author/author';
import { Book } from '../../../types/book/book';
import { fromTrigger } from '../../../utils/notifications/from-trigger';
import { AddBookSchema } from '../../../utils/validation-schemas/book/add-book.schema';
import { BookSaveForm } from '../../books/book-save-form/book-save-form';
import { FormikDialog } from '../../shared/formik-dialog/formik-dialog';
import { FormikExtended } from '../../shared/formik-extended/formik-extended';
import { ListHeader } from '../../shared/list-header/list-header';
import { ScrollableList } from '../../shared/scrollable-list/scrollable-list';
import { AuthorBook } from '../author-book/author-book';

export const AuthorBooks: FC<Props> = ({ author }) => {
    const ADD_BOOK_INITIAL_VALUES: Book = {
        id: 0,
        title: '',
        copiesCount: 0,
        author,
    };

    const { data: books, isFetching } = useGetBookByAuthorQuery(author.id);
    const [addBook] = useAddBookMutation();

    const { openDialog, closeDialog } = useDialog();

    const openAddDialog = () => {
        openDialog(
            <FormikExtended
                initialValues={ADD_BOOK_INITIAL_VALUES}
                onSubmit={handleAddBook}
                validationSchema={AddBookSchema}
                disabled={{ author: { id: true, firstName: true, lastName: true } }}
            >
                <FormikDialog title="הוספת ספר">
                    <BookSaveForm />
                </FormikDialog>
            </FormikExtended>,
        );
    };

    const handleAddBook = (book: Book, { setSubmitting }: FormikHelpers<Book>) => {
        fromTrigger(addBook(book))
            .then(closeDialog)
            .finally(() => setSubmitting(false));
    };

    return (
        <Box display="flex" flexDirection="column" height='inherit'>
            <ListHeader
                title={`הספרים של ${author.firstName} ${author.lastName}`}
                actionName="הוסף ספר" onClick={openAddDialog}
                disabled={isFetching}
            />
            <ScrollableList>
                {books?.map((book) => (
                    <ListItem key={book.id}>
                        <AuthorBook book={book} />
                    </ListItem>
                ))}
            </ScrollableList>
        </Box>
    );
};

interface Props {
    author: Author;
}
