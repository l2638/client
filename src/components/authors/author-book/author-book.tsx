import { FC } from 'react';
import { BookPreview } from '../../../types/book/book.preview';
import { InfoCard } from '../../shared/info-card/info-card';

export const AuthorBook: FC<Props> = ({ book }) => (
  <InfoCard title={book.title} subheader={`מזהה: ${book.id}`} />
);

interface Props {
    book: BookPreview;
}
