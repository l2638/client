import { Box, ListItem } from '@mui/material';
import { Formik, FormikHelpers } from 'formik';
import { FC } from 'react';
import { useAddAuthorMutation, useDeleteAuthorMutation, useGetAuthorsQuery, useUpdateAuthorMutation } from '../../../apis/author.api';
import { useDialog } from '../../../hooks/use-dialog';
import { Author } from '../../../types/author/author';
import { AuthorCreate } from '../../../types/author/author.create';
import { AuthorDetailed } from '../../../types/author/author.detailed';
import { fromTrigger } from '../../../utils/notifications/from-trigger';
import { AuthorSchema } from '../../../utils/validation-schemas/author/author.schema';
import { FormikDialog } from '../../shared/formik-dialog/formik-dialog';
import { ListHeader } from '../../shared/list-header/list-header';
import { ScrollableList } from '../../shared/scrollable-list/scrollable-list';
import { AuthorCard } from '../author-card/author-card';
import { AuthorDeleteDialog } from '../author-delete-dialog/author-delete-dialog';
import { AuthorSaveForm } from '../author-save-form/author-save-form';

export const AuthorList: FC<Props> = ({ onClick, selectedAuthor }) => {
    const { data: authors } = useGetAuthorsQuery();

    const [addAuthor] = useAddAuthorMutation();
    const [updateAuthor] = useUpdateAuthorMutation();
    const [deleteAuthor] = useDeleteAuthorMutation();

    const { openDialog, closeDialog } = useDialog();

    const handleClick = (author: Author | null): void => {
        closeDialog();
        onClick(author);
    };

    const openDeleteDialog = (author: AuthorDetailed): void => {
        openDialog(
            <AuthorDeleteDialog
                author={author}
                onApprove={handleDeleteAuthor}
            />
        );
    };

    const openEditDialog = (author: Author): void => {
        openDialog(
            <Formik
                initialValues={author}
                onSubmit={handleEditAuthor}
                validationSchema={AuthorSchema}
            >
                <FormikDialog title="עריכת סופר">
                    <AuthorSaveForm />
                </FormikDialog>
            </Formik>,
        );
    };

    const openAddDialog = (): void => {
        openDialog(
            <Formik
                initialValues={{ firstName: '', lastName: '' }}
                onSubmit={handleAddAuthor}
                validationSchema={AuthorSchema}
            >
                <FormikDialog title="הוספת סופר">
                    <AuthorSaveForm />
                </FormikDialog>
            </Formik>,
        );
    };

    const handleEditAuthor = (author: Author, { setSubmitting }: FormikHelpers<Author>): void => {
        fromTrigger(updateAuthor({
            id: author.id,
            author: author,
        }))
            .then(handleClick)
            .finally(() => setSubmitting(false));
    };

    const handleAddAuthor = (author: AuthorCreate, { setSubmitting }: FormikHelpers<AuthorCreate>): void => {
        fromTrigger(addAuthor(author))
            .then(handleClick)
            .finally(() => setSubmitting(false));
    };

    const handleDeleteAuthor = (author: Author): void => {
        fromTrigger(deleteAuthor(author.id))
            .then(() => handleClick(null));
    };

    return (
        <Box display="flex" flexDirection="column" height='inherit'>
            <ListHeader title="רשימת הסופרים" actionName="הוסף סופר" onClick={openAddDialog} />
            <ScrollableList>
                {authors?.map((author) => (
                    <ListItem key={author.id}>
                        <AuthorCard
                            author={author}
                            onClick={handleClick}
                            selected={author.id === selectedAuthor?.id}
                            onEdit={openEditDialog}
                            onDelete={openDeleteDialog}
                        />
                    </ListItem>
                ))}
            </ScrollableList>
        </Box>
    );
};

interface Props {
    onClick: (author: Author | null) => void;
    selectedAuthor: Author | null;
}
