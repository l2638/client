import { FC } from 'react';
import { Author } from '../../../types/author/author';
import { AreYouSureDialog } from '../../shared/are-you-sure-dialog/are-you-sure-dialog';

export const AuthorDeleteDialog: FC<Props> = ({ author, onApprove }) => {
  return (
    <AreYouSureDialog
      title={`האם הינך בטוח שברצונך למחוק את הסופר/ת ${author?.firstName} ${author?.lastName}?`}
      content="לא ניתן להחזיר את הפעולה לאחור!"
      onApprove={() => onApprove(author)}
    />
  );
};

interface Props {
    author: Author;
    onApprove: (author: Author) => void;
}
