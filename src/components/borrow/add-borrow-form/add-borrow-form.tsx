import { Autocomplete, CircularProgress, Grid, TextField, useAutocomplete } from '@mui/material';
import { FC, SyntheticEvent } from 'react';
import { useGetAvailableBooksQuery } from '../../../apis/book.api';
import { useGetUsersQuery } from '../../../apis/user.api';
import { useFormikContextExtended } from '../../../hooks/use-formik-context-extended';
import { BookPreview } from '../../../types/book/book.preview';
import { AddBorrowFormData as Borrow } from '../../../types/borrow/add-borrow-form-data';
import { User } from '../../../types/user/user';

export const AddBorrowForm: FC = () => {

    const {
        setFieldValue,
        values,
        touched,
        errors,
        disabled
    } = useFormikContextExtended<Borrow>();

    const { data: books = [], isLoading: areBooksLoading } = useGetAvailableBooksQuery(undefined, { skip: !!disabled.book });
    const { data: users = [], isLoading: areUsersLoading } = useGetUsersQuery(undefined, { skip: !!disabled.user });

    const handleUserChange = (_event: SyntheticEvent, user: User): void => {
        setFieldValue('user', user);
    };

    const handleBookChange = (_event: SyntheticEvent, book: BookPreview): void => {
        setFieldValue('book', book);
    };

    return (
        <Grid container spacing={1}>
            <Grid item xs={8}>
                <Autocomplete
                    options={users}
                    getOptionLabel={(option) => option.id + ' - ' + option.firstName + ' ' + option.lastName}
                    disabled={!!disabled.user || areUsersLoading}
                    value={values.user}
                    ListboxProps={{ style: { maxHeight: 200, overflowY: 'auto' } }}
                    onChange={handleUserChange}
                    disableClearable
                    renderInput={(params) => <TextField
                        value={values.user}
                        {...params}
                        label="משתמש"
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <>
                                    {areUsersLoading && <CircularProgress color="inherit" size={20} />}
                                    {params.InputProps.endAdornment}
                                </>
                            ),
                        }}
                        error={touched.user && !!errors.user}
                        helperText={touched.user && errors.user || ' '}
                    />}
                    noOptionsText="אין תוצאות"
                />
            </Grid>

            <Grid item xs={6}>
                <Autocomplete
                    options={books}
                    getOptionLabel={(option) => option.title}
                    disabled={!!disabled.book || areBooksLoading}
                    value={values.book}
                    ListboxProps={{ style: { maxHeight: 200, overflowY: 'auto' } }}
                    onChange={handleBookChange}
                    disableClearable
                    renderInput={(params) => <TextField
                        value={values.book}
                        {...params}
                        label="ספר"
                        InputProps={{
                            ...params.InputProps,
                            endAdornment: (
                                <>
                                    {areBooksLoading && <CircularProgress color="inherit" size={20} />}
                                    {params.InputProps.endAdornment}
                                </>
                            ),
                        }}
                        error={touched.book && !!errors.book}
                        helperText={touched.book && errors.book || ' '}
                    />}
                    renderOption={(props, option) => (
                        <li {...props} key={option.id}>
                            {option.title}
                        </li>
                    )}
                    noOptionsText="אין תוצאות"
                />
            </Grid>
        </Grid>
    );
};