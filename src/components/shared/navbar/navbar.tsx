import { Box, List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { NAVBAR_ITEMS } from '../../../constants/navbar/navbar-items';
import styles from './navbar.module.css';


export const Navbar = () => {
  const router = useRouter();

  return (
    <Box width='250px'>
      <List>
        {NAVBAR_ITEMS.map((navbarItem) => (
          <Link key={navbarItem.pathname} href={navbarItem.pathname} passHref={true}>
            <ListItem button component='a' className={router.pathname === navbarItem.pathname ? styles.active : ''}>
              <ListItemIcon>
                {navbarItem.icon}
              </ListItemIcon>
              <ListItemText primary={navbarItem.title} />
            </ListItem>
          </Link>
        ))}
      </List>
    </Box>
  );
};
