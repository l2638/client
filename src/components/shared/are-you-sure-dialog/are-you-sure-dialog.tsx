import { Button, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@mui/material';
import { FC } from 'react';
import { useDialog } from '../../../hooks/use-dialog';

export const AreYouSureDialog: FC<Props> = ({ title, content, onApprove, isSubmitting }) => {
  const { closeDialog } = useDialog();

  return (
    <>
      <DialogTitle>
        {title}
      </DialogTitle>
      {content &&
                <DialogContent>
                  <DialogContentText>
                    {content}
                  </DialogContentText>
                </DialogContent>
      }
      <DialogActions>
        <Button onClick={closeDialog}>ביטול</Button>
        <Button variant="contained" onClick={onApprove} disabled={isSubmitting} autoFocus>אישור</Button>
      </DialogActions>
    </>
  );
};

interface Props {
    title: string;
    content?: string;
    onApprove: () => void;
    isSubmitting?: boolean
}
