import { List } from '@mui/material';
import { styled } from '@mui/system';

export const ScrollableList = styled(List)({
  overflowY: 'auto',
  flexGrow: 1,
  flexShrink: 1,
});
