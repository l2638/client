import { AppBar, Box, Toolbar, Typography } from '@mui/material';
import { FC, MutableRefObject } from 'react';

export const Header: FC<Props> = ({ headerRef }) => {
  return (
    <Box sx={{ flexGrow: 1 }} ref={headerRef}>
      <AppBar position="static" dir="rtl" sx={{ backgroundColor: 'white' }}>
        <Toolbar>
          <Typography color={'black'} variant="h4" component="div" sx={{ flexGrow: 1 }}>
            הספרייה
          </Typography>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

interface Props {
  headerRef?: MutableRefObject<HTMLDivElement | undefined>;
}