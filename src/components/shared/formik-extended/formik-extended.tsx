import { Formik, FormikConfig } from 'formik';
import { FormikExtendedContext } from '../../../contexts/formik-extended.context';
import { FormikDisabled } from '../../../types/formik-extended/formik-disabled';

export function FormikExtended<Values>(props: FormikExtendedProps<Values>) {
  return (
    <FormikExtendedContext.Provider value={props.disabled}>
      <Formik {...props} />
    </FormikExtendedContext.Provider>
  );
}

interface FormikExtendedProps<Values> extends FormikConfig<Values> {
    disabled: FormikDisabled<Values>;
}
