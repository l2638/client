import createCache from '@emotion/cache';
import { CacheProvider } from '@emotion/react';
import { FC, ReactNode } from 'react';
import { prefixer } from 'stylis';
import rtlPlugin from 'stylis-plugin-rtl';

const cacheRtl = createCache({
  key: 'muirtl',
  stylisPlugins: [prefixer, rtlPlugin],
});

export const RTL: FC<Props> = ({ children }) => {
  return <CacheProvider value={cacheRtl}>{children}</CacheProvider>;
}

interface Props {
  children: ReactNode;
}
