import { Box } from '@mui/material';
import { FC, ReactNode, useEffect, useRef, useState } from 'react';
import { Provider } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import store from '../../../store/store';
import { DialogProvider } from '../../../utils/providers/dialog.provider';
import { Header } from '../header/header';
import { Navbar } from '../navbar/navbar';

export const BasicLayout: FC<Props> = ({ children }) => {

  const ref = useRef<HTMLDivElement>();

  const [headerHeight, setHeaderHeight] = useState<number>();

  useEffect(() => {
    setHeaderHeight(ref.current?.clientHeight);
  }, []);

  return (
    <Provider store={store}>
      <DialogProvider>
        <Header headerRef={ref} />
        <Box display='flex' height={`calc(100% - ${headerHeight}px)`}>
          <Navbar></Navbar>
          {children}
        </Box>
        <ToastContainer position="bottom-left" rtl />
      </DialogProvider>
    </Provider>
  );
};

interface Props {
  children: ReactNode
}
