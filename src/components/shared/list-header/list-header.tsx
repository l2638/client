import { Box, Button, Tooltip, Typography } from '@mui/material';
import { FC, MouseEvent } from 'react';
import styles from './list-header.module.css';

export const ListHeader: FC<Props> = ({ title, actionName, onClick, disabled, tooltip = '' }) => {
  return (
    <Box display='flex' padding={2}>
      <Typography variant="h5" component="div" flexGrow={1}>
        {title}
      </Typography>
      <Tooltip title={tooltip}>
        <span className={disabled ? styles.disabled: ''}>
          <Button variant="contained" onClick={onClick} disabled={disabled}>
            {actionName}
          </Button>
        </span>
      </Tooltip>
    </Box>
  );
};

interface Props {
    title: string;
    actionName: string;
    onClick: (event: MouseEvent<HTMLButtonElement>) => void;
    disabled?: boolean;
    tooltip?: string;
}
