import { CardActions, CardHeader } from '@mui/material';
import { Box } from '@mui/system';
import { ReactNode } from 'react';

export const InfoCardContent = (props: InfoCardContentProps) => {
  return (
    <Box display={'flex'}>
      <CardHeader sx={{ flexGrow: 1 }}
        title={props.title}
        subheader={props.subheader}>
      </CardHeader>
      <CardActions>
        {props.children}
      </CardActions>
    </Box>
  );
};

export interface InfoCardContentProps {
    title: string;
    subheader?: string | number;
    children?: ReactNode;
}
