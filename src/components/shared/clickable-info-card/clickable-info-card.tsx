import { Card, CardActionArea } from '@mui/material';
import { MouseEvent } from 'react';
import { InfoCardContent, InfoCardContentProps } from '../info-card-content/info-card-content';

export const ClickableInfoCard = (props: ClickableInfoCardProps) => {
  return (
    <Card sx={{ backgroundColor: props.selected ? '#00000014' : '', width: 'inherit' }} variant="outlined">
      <CardActionArea onClick={props.onClick} component='div'>
        <InfoCardContent title={props.title} subheader={props.subheader}>
          {props.children}
        </InfoCardContent>
      </CardActionArea>
    </Card>
  );
};

interface ClickableInfoCardProps extends InfoCardContentProps {
    onClick: (event: MouseEvent) => void;
    selected?: boolean;
}
