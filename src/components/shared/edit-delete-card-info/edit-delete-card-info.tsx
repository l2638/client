import EditIcon from '@mui/icons-material/Edit';
import { IconButton, Tooltip } from '@mui/material';
import { MouseEvent } from 'react';
import { ClickableInfoCard } from '../clickable-info-card/clickable-info-card';
import { DeleteButton } from '../delete-button/delete-button';

export const EditDeleteCard = (props: EditDeleteCardProps) => {
  const handleEdit = (event: MouseEvent): void => {
    stopPropagation(event);
    props.onEdit?.();
  };

  const handleDelete = (event: MouseEvent): void => {
    stopPropagation(event);
    props.onDelete?.();
  };

  const handleClick = (): void => {
    props.onClick();
  };

  const stopPropagation = (event: MouseEvent) => event.stopPropagation();

  return (
    <ClickableInfoCard title={props.title} subheader={props.subheader} selected={props.selected} onClick={handleClick}>
      <>
        {props.onEdit &&
          <Tooltip title="ערוך">
            <IconButton size="large" onClick={handleEdit} onMouseDown={stopPropagation}>
              <EditIcon fontSize="inherit" />
            </IconButton>
          </Tooltip>
        }
        {props.onDelete &&
          <Tooltip title="מחק">
            <DeleteButton onClick={handleDelete} onMouseDown={stopPropagation} />
          </Tooltip>
        }
      </>
    </ClickableInfoCard>
  );
};

interface EditDeleteCardProps {
  title: string;
  subheader?: string;
  onClick: () => void;
  onEdit?: () => void;
  onDelete?: () => void;
  selected?: boolean;
}
