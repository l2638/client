import { Button, DialogActions, DialogContent, DialogTitle } from "@mui/material";
import { Box } from "@mui/system";
import { useFormikContext } from "formik";
import { FC, ReactNode } from "react";
import { useDialog } from "../../../hooks/use-dialog";

export const FormikDialog: FC<Props> = ({ title, children }) => {

    const {
        isSubmitting,
        handleSubmit,
    } = useFormikContext();

    const { closeDialog } = useDialog();

    return (
        <>
            <DialogTitle>
                {title}
            </DialogTitle>

            <Box component="form" onSubmit={handleSubmit}>
                <DialogContent>
                    {children}
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDialog}>ביטול</Button>
                    <Button type="submit" variant="contained" disabled={isSubmitting}>אישור</Button>
                </DialogActions>
            </Box>
        </>
    );
}

interface Props {
    title: string;
    children: ReactNode
}