import DeleteIcon from '@mui/icons-material/Delete';
import { IconButton } from '@mui/material';
import { FC, MouseEvent } from 'react';

export const DeleteButton: FC<DeleteButtonProps> = ({ onClick, onMouseDown, disabled }) => (
  <IconButton size="large" sx={{ color: '#E8223A' }} onClick={onClick} disabled={disabled} onMouseDown={onMouseDown}>
    <DeleteIcon fontSize="inherit" />
  </IconButton>
);

interface DeleteButtonProps {
    onClick: (event: MouseEvent) => void;
    onMouseDown?: (event: MouseEvent) => void;
    disabled?: boolean;
}
