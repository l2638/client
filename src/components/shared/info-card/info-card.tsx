import { Card } from '@mui/material';
import { InfoCardContent, InfoCardContentProps } from '../info-card-content/info-card-content';

export const InfoCard = (props: InfoCardProps) => {
  return (
    <Card sx={{ width: 'inherit' }} variant="outlined">
      <InfoCardContent title={props.title} subheader={props.subheader}>
        {props.children}
      </InfoCardContent>
    </Card>
  );
};

type InfoCardProps = InfoCardContentProps
