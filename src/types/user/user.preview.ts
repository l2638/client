export interface UserPreview {
    id: number;
    firstName: string;
    lastName: string;
}