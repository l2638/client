export interface UserCreate {
    id: number;
    firstName: string;
    lastName: string;
}