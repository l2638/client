export interface AuthorCreate {
    firstName: string;
    lastName: string;
}