export interface AuthorDetailed {
    id: number;
    firstName: string;
    lastName: string;
    booksCount: number
}