export interface AuthorUpdate {
    firstName: string;
    lastName: string;
}