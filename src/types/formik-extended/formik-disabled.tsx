export type FormikDisabled<Values> = {
    [K in keyof Values]?: Values[K] extends any[] ? Values[K][number] extends object ? FormikDisabled<Values[K][number]>[] : boolean : Values[K] extends object ? FormikDisabled<Values[K]> : boolean;
}