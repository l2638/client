import { FormikContextType } from "formik"
import { FormikDisabled } from "./formik-disabled"

export type FormikExtendedContextType<Values> = FormikContextType<Values> & {
    disabled: FormikDisabled<Values>
}