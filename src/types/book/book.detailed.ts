export interface BookDetailed {
    id: number;
    title: string;
    author: string;
    isStarred: boolean;
    isActive: boolean;
}