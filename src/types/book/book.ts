import { Author } from "../author/author";

export interface Book {
    id: number;
    title: string;
    author: Author;
    copiesCount: number;
}