import { Author } from "../author/author";

export interface BookCreate {
    author: Author;
    title: string;
    copiesCount: number;
}