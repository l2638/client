
export interface BookPreview {
    id: number;
    title: string;
}