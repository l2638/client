import { Author } from "../author/author";

export interface BookUpdate {
    author: Author;
    copiesCount: number;
}