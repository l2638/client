export interface DialogOptions {
    width?: "xl" | "lg" | "md" | "sm";
}