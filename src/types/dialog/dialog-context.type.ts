import { ReactNode } from "react";
import { DialogOptions } from "./dialog-options";

export interface DialogContextType {
    openDialog: (component: ReactNode, options?: DialogOptions) => void;
    closeDialog: () => void;
}