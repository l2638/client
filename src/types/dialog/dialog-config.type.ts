import { ReactNode } from "react";
import { DialogOptions } from "./dialog-options";

export type DialogConfig = {
    component: ReactNode;
    options?: DialogOptions;
};