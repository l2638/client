export interface BorrowCreate {
    userId: number;
    bookId: number;
}