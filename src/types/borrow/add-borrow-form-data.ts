import { BookPreview } from "../book/book.preview";
import { UserPreview } from "../user/user.preview";

export interface AddBorrowFormData {
    book: BookPreview;
    user: UserPreview;
}