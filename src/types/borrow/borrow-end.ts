export interface BorrowEnd {
    userId: number;
    bookId: number;
}