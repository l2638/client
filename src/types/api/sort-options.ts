export interface SortOptions<T> {
    sort?: (keyof T)[];
}