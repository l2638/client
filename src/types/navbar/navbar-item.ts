export interface NavbarItem {
    title: string;
    pathname: string;
    icon: JSX.Element
}