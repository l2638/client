import * as Yup from "yup";
import { Book } from "../../../types/book/book";
import { AuthorSchema } from "../author/author.schema";

export const getEditBookSchema = (book: Book) => Yup.object({
    title: Yup
        .string()
        .required("שם של ספר הוא חובה")
        .max(100, "שם של ספר לא יכול להיות יותר מ-100 תווים"),
    copiesCount: Yup
        .number()
        .min(book.copiesCount, "לא ניתן להקטין את כמות העותקים של הספר"),
    author: AuthorSchema
});