import * as Yup from "yup";
import { AuthorSchema } from "../author/author.schema";

export const AddBookSchema = Yup.object({
    title: Yup
        .string()
        .required("שם של ספר הוא חובה")
        .max(100, "שם של ספר לא יכול להיות יותר מ-100 תווים"),
    copiesCount: Yup
        .number()
        .min(0, "לספר לא יכול להיות מספר שלילי של עותקים"),
    author: AuthorSchema
})