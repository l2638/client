import * as Yup from "yup";

export const UserSchema = Yup.object({
    id: Yup
        .number()
        .required('תעודת זהות היא חובה')
        .min(10, 'תעודת זהות לא תקינה')
        .max(999999999, 'תעודת זהות לא תקינה'),
    firstName: Yup
        .string()
        .required('שם פרטי הוא חובה')
        .max(50, 'שם פרטי לא יכול ליהות ארוך יותר מ-50 תווים'),
    lastName: Yup
        .string()
        .required('שם משפחה הוא חובה')
        .max(50, 'שם משפחה לא יכול להיות ארוך יותר מ-50 תווים')
});