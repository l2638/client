import * as Yup from "yup";

export const AuthorSchema = Yup
    .object({
        firstName: Yup
            .string()
            .required('שם פרטי הוא חובה')
            .max(50, 'שם פרטי לא יכול ליהות ארוך יותר מ-50 תווים'),
        lastName: Yup
            .string()
            .required('שם משפחה הוא חובה')
            .max(50, 'שם משפחה לא יכול להיות ארוך יותר מ-50 תווים')
    })