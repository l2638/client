import * as Yup from 'yup';

export const AddBorrowSchema = Yup.object({
    user: Yup
        .object()
        .nullable()
        .required('משתמש הוא חובה'),
    book: Yup
        .object()
        .nullable()
        .required('ספר הוא חובה'),
});