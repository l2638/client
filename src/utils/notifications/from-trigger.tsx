import { MutationDefinition } from "@reduxjs/toolkit/dist/query";
import { MutationActionCreatorResult } from "@reduxjs/toolkit/dist/query/core/buildInitiate";
import { toast, ToastOptions, ToastPromiseParams } from "react-toastify";
import { DefaultErrorMessage } from "../../constants/default-error-message/default-error-message";

const DEFAULT_PROMISE_OPTIONS: ToastPromiseParams = {
    pending: "טוען...",
    error: {
        render: <DefaultErrorMessage />,
        delay: 100
    },
    success: {
        render: "בקשה הושלמה בהצלחה",
        delay: 100
    }
}

export function fromTrigger<T, K>(
    trigger: MutationActionCreatorResult<MutationDefinition<T, any, any, K, any>>,
    promiseParams?: ToastPromiseParams,
    options?: ToastOptions): Promise<K> {

    return toast.promise(
        trigger.unwrap(),
        { ...DEFAULT_PROMISE_OPTIONS, ...promiseParams },
        options
    );
}