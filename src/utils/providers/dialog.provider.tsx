import { Dialog } from "@mui/material";
import { FC, ReactNode, useEffect, useState } from "react";
import { DialogContext } from "../../contexts/dialog.context";
import { DialogConfig } from "../../types/dialog/dialog-config.type";
import { DialogOptions } from "../../types/dialog/dialog-options";

export const DialogProvider: FC<Props> = ({ children }) => {

    const [dialog, setDialog] = useState<DialogConfig>();

    const openDialog = (component: ReactNode, options?: DialogOptions) => setDialog({ component, options })

    const [isOpen, setOpen] = useState(false);

    useEffect(() => {
        setOpen(!!dialog);
    }, [dialog])

    const closeDialog = () => setOpen(false);

    return (
        <DialogContext.Provider value={{ closeDialog, openDialog }} >
            <Dialog
                open={isOpen}
                onClose={closeDialog}
                maxWidth={dialog?.options?.width}
                fullWidth
            >
                {dialog?.component}
            </Dialog>
            {children}
        </DialogContext.Provider>
    )
}

interface Props {
    children: ReactNode;
}