import { createContext } from "react";
import { FormikDisabled } from "../types/formik-extended/formik-disabled";

export const FormikExtendedContext = createContext<FormikDisabled<any>>({});