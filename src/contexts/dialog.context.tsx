import { createContext } from "react";
import { DialogContextType } from "../types/dialog/dialog-context.type";

export const DialogContext = createContext<DialogContextType>({
    openDialog: () => { },
    closeDialog: () => { }
});