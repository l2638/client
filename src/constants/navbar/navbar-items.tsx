import BookIcon from '@mui/icons-material/Book';
import UserIcon from '@mui/icons-material/PermIdentity';
import AuthorIcon from '@mui/icons-material/School';
import { NavbarItem } from '../../types/navbar/navbar-item';

export const NAVBAR_ITEMS: NavbarItem[] = [
    {
        title: 'משתמשים',
        pathname: '/users',
        icon: <UserIcon />
    },
    {
        title: 'ספרים',
        pathname: '/books',
        icon: <BookIcon />
    },
    {
        title: 'סופרים',
        pathname: '/authors',
        icon: <AuthorIcon />
    }
]