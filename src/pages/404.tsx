import { Box, Typography } from '@mui/material';

export default function NotFound() {
  return (
    <Box width="100%" mt={10}>
      <Typography variant="h1" component="div" textAlign="center">עמוד לא נמצא :(</Typography>
    </Box>
  );
}
