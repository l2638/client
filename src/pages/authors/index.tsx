import { Grid } from '@mui/material';
import { useState } from 'react';
import { AuthorBooks } from '../../components/authors/author-books/author-books';
import { AuthorList } from '../../components/authors/author-list/author-list';
import { Author } from '../../types/author/author';

const Authors = () => {
  const [selectedAuthor, setSelectedAuthor] = useState<Author | null>(null);

  return (
    <Grid container>
      <Grid item xs={6} height='100%'>
        <AuthorList onClick={setSelectedAuthor} selectedAuthor={selectedAuthor} />
      </Grid>
      {selectedAuthor &&
        <Grid item xs={6} height='100%'>
          <AuthorBooks author={selectedAuthor} />
        </Grid>
      }
    </Grid>
  );
};

export default Authors;
