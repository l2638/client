import { Grid } from '@mui/material';
import { useState } from 'react';
import { BookList } from '../../components/books/book-list/book-list';
import { BookReaders } from '../../components/books/book-readers/book-readers';
import { Book } from '../../types/book/book';

const Books = () => {
  const [selectedBook, setSelectedBook] = useState<Book | null>(null);

  return (
    <Grid container>
      <Grid item xs={6} height='100%'>
        <BookList onClick={setSelectedBook} selectedBook={selectedBook} />
      </Grid>
      {selectedBook &&
        <Grid item xs={6} height='100%'>
          <BookReaders book={selectedBook} />
        </Grid>
      }
    </Grid>
  );
};

export default Books;
