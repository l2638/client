import { Box } from '@mui/material';
import type { NextPage } from 'next';
import Image from 'next/image';
import home from '../../public/home.webp';

const Home: NextPage = () => {
  return (
    <Box width="100%" mt={10} textAlign="center">
      <Image
        src={home}
        alt="Home screen gif"
      />
    </Box>
  );
};

export default Home;
