import { ThemeProvider } from '@mui/material';
import type { AppProps } from 'next/app';
import '../../styles/globals.css';
import { Theme } from '../../styles/theme';
import { BasicLayout } from '../components/shared/basic-layout/basic-layout';
import { RTL } from '../components/shared/rtl/rtl';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <RTL>
      <ThemeProvider theme={Theme}>
        <BasicLayout>
          <Component {...pageProps} />
        </BasicLayout>
      </ThemeProvider>
    </RTL>
  );
}

export default MyApp;
