import { Grid } from '@mui/material';
import { useState } from 'react';
import { UserBooks } from '../../components/users/user-books/user-books';
import { UsersList } from '../../components/users/users-list/users-list';
import { User } from '../../types/user/user';

const Users = () => {
  const [selectedUser, setSelectedUser] = useState<User | null>(null);

  return (
    <Grid container>
      <Grid item xs={6} height='100%'>
        <UsersList onClick={setSelectedUser} selectedUser={selectedUser} />
      </Grid>
      {selectedUser &&
        <Grid item xs={6} height='100%'>
          <UserBooks user={selectedUser} />
        </Grid>
      }
    </Grid>
  );
};

export default Users;
