import { TagDescription } from "@reduxjs/toolkit/dist/query/endpointDefinitions";
import { createApi, fetchBaseQuery, FetchBaseQueryError } from "@reduxjs/toolkit/query/react";

export const baseApi = createApi({
    baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080' }),
    endpoints: () => ({}),
    tagTypes: ['Book', 'User', 'Author', 'UserBooks', 'BookReaders', 'Borrow', 'AuthorBooks']
});

export const ignoreOnError = <T extends string> (error?: FetchBaseQueryError, ...tags: TagDescription<T>[]): TagDescription<T>[] => error ? [] : tags;