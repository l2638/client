import { Book } from '../types/book/book';
import { BookCreate } from '../types/book/book.create';
import { BookDetailed } from '../types/book/book.detailed';
import { BookPreview } from '../types/book/book.preview';
import { BookUpdate } from '../types/book/book.update';
import { baseApi, ignoreOnError } from './base.api';

const BASE_URI = 'books';

export const bookApi = baseApi.injectEndpoints({
    endpoints: build => ({
        getBookByUser: build.query<BookDetailed[], number>({
            query: userId => ({
                url: BASE_URI,
                params: { userId }
            }),
            providesTags: (books = [], _error, userId) => [
                { type: 'UserBooks', id: userId },
                ...books.map(({ id }) => ({ type: 'Book' as const, id }))
            ]
        }),
        getBookByAuthor: build.query<BookPreview[], number>({
            query: authorId => ({
                url: `${BASE_URI}/previews`,
                params: { authorId }
            }),
            providesTags: (books = [], _error, authorId) => [
                { type: 'AuthorBooks', id: authorId },
                ...books.map(({ id }) => ({ type: 'Book' as const, id }))
            ]
        }),
        getAvailableBooks: build.query<BookPreview[], void>({
            query: () => `${BASE_URI}/available`,
            providesTags: (books = []) => [
                'Borrow',
                'Book',
                ...books.map(({ id }) => ({ type: 'Book' as const, id }))
            ]
        }),
        getBooks: build.query<Book[], void>({
            query: () => BASE_URI,
            providesTags: (books = []) => [
                'Book',
                ...books.map(({ id }) => ({ type: 'Author' as const, id })),
                ...books.map(({ author }) => ({ type: 'Author' as const, id: author.id })),
            ]
        }),
        addBook: build.mutation<Book, BookCreate>({
            query: book => ({
                url: BASE_URI,
                method: 'POST',
                body: book
            }),
            invalidatesTags: (book, error, { author }) => ignoreOnError(error,
                'Book',
                author.id
                    ? { type: 'Author', id: book?.author.id }
                    : 'Author',
                { type: 'AuthorBooks', id: book?.author.id }
            )
        }),
        updateBook: build.mutation<Book, { id: number, book: BookUpdate }>({
            query: ({ id, book }) => ({
                url: `${BASE_URI}/${id}`,
                method: 'PUT',
                body: book
            }),
            invalidatesTags: (result, error, { id, book }) => ignoreOnError(error,
                { type: 'Book', id },
                book.author.id
                    ? { type: 'Author', id: book.author.id }
                    : 'Author',
                { type: 'AuthorBooks', id: result?.author.id }
            )
        })
    }),
    overrideExisting: false
});

export const {
    useGetBookByUserQuery,
    useGetAvailableBooksQuery,
    useGetBooksQuery,
    useAddBookMutation,
    useUpdateBookMutation,
    useGetBookByAuthorQuery
} = bookApi;