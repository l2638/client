import { StarCreate } from '../types/star/star.create';
import { baseApi, ignoreOnError } from './base.api';

const BASE_URI = 'starredBooks';

export const starredBookApi = baseApi.injectEndpoints({
    endpoints: build => ({
        addStar: build.mutation<void, StarCreate>({
            query: starDetails => ({
                url: BASE_URI,
                method: 'POST',
                body: starDetails
            }),
            invalidatesTags: (_result, error, { userId }) => ignoreOnError(error, { type: 'UserBooks', id: userId })
        }),
        removeStar: build.mutation<void, { userId: number, bookId: number }>({
            query: ({ userId, bookId }) => ({
                url: BASE_URI,
                params: { userId, bookId },
                method: 'DELETE'
            }),
            invalidatesTags: (_result, error, { userId }) => ignoreOnError(error, { type: 'UserBooks', id: userId })
        })
    }),
    overrideExisting: false
});

export const {
    useAddStarMutation,
    useRemoveStarMutation
} = starredBookApi;