import { Borrow } from '../types/borrow/borrow';
import { BorrowEnd } from '../types/borrow/borrow-end';
import { BorrowCreate } from '../types/borrow/borrow.create';
import { baseApi, ignoreOnError } from './base.api';

const BASE_URI = 'borrows';

export const borrowApi = baseApi.injectEndpoints({
    endpoints: build => ({
        addBorrow: build.mutation<Borrow, BorrowCreate>({
            query: borrow => ({
                url: BASE_URI,
                method: 'POST',
                body: borrow
            }),
            invalidatesTags: (_result, error, borrow) => ignoreOnError(error,
                'Borrow',
                { type: 'BookReaders', id: borrow.bookId },
                { type: 'UserBooks', id: borrow.userId },
            )
        }),
        endBorrow: build.mutation<void, BorrowEnd>({
            query: borrow => ({
                url: `${BASE_URI}/end`,
                method: 'PUT',
                body: borrow
            }),
            invalidatesTags: (_result, error, borrow) => ignoreOnError(error,
                'Borrow',
                { type: 'BookReaders', id: borrow.bookId },
                { type: 'UserBooks', id: borrow.userId },
            )
        })
    }),
    overrideExisting: false
});

export const {
    useAddBorrowMutation,
    useEndBorrowMutation
} = borrowApi;