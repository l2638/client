import { Author } from '../types/author/author';
import { AuthorCreate } from '../types/author/author.create';
import { AuthorDetailed } from '../types/author/author.detailed';
import { AuthorUpdate } from '../types/author/author.update';
import { baseApi, ignoreOnError } from './base.api';

const BASE_URI = 'authors';

export const authorApi = baseApi.injectEndpoints({
    endpoints: build => ({
        getAuthorsPreviews: build.query<Author[], void>({
            query: () => `${BASE_URI}/previews`,
            providesTags: (authors = []) => [
                'Author',
                ...authors.map(({ id }) => ({ type: 'Author' as const, id }))
            ]
        }),
        getAuthors: build.query<AuthorDetailed[], void>({
            query: () => BASE_URI,
            providesTags: (authors = []) => [
                'Author',
                ...authors.map(({ id }) => ({ type: 'Author' as const, id }))
            ]
        }),
        addAuthor: build.mutation<AuthorDetailed, AuthorCreate>({
            query: author => ({
                url: BASE_URI,
                method: 'POST',
                body: author
            }),
            invalidatesTags: (_result, error) => ignoreOnError(error, 'Author')
        }),
        updateAuthor: build.mutation<AuthorDetailed, { id: number, author: AuthorUpdate }>({
            query: ({ author, id }) => ({
                url: `${BASE_URI}/${id}`,
                method: 'PUT',
                body: author
            }),
            invalidatesTags: (_result, error, { id }) => ignoreOnError(error, { type: 'Author', id })
        }),
        deleteAuthor: build.mutation<void, number>({
            query: id => ({
                url: `${BASE_URI}/${id}`,
                method: 'DELETE'
            }),
            invalidatesTags: (_result, error, id) => ignoreOnError(error, { type: 'Author', id })
        })
    }),
    overrideExisting: false
});

export const {
    useGetAuthorsQuery,
    useAddAuthorMutation,
    useUpdateAuthorMutation,
    useDeleteAuthorMutation,
    useGetAuthorsPreviewsQuery
} = authorApi;