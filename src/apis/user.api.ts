import { SortOptions } from '../types/api/sort-options';
import { User } from '../types/user/user';
import { UserCreate } from '../types/user/user.create';
import { UserPreview } from '../types/user/user.preview';
import { UserUpdate } from '../types/user/user.update';
import { baseApi, ignoreOnError } from './base.api';

const BASE_URI = 'users';

export const userApi = baseApi.injectEndpoints({
    endpoints: build => ({
        getUsers: build.query<User[], SortOptions<User> | undefined>({
            query: options => ({
                url: BASE_URI,
                params: options
            }),
            providesTags: (users = []) => [
                'User',
                ...users.map(({ id }) => ({ type: 'User' as const, id }))
            ]
        }),
        getUsersByBook: build.query<UserPreview[], number>({
            query: bookId => ({
                url: BASE_URI,
                params: { bookId }
            }),
            providesTags: (users = [], _error, bookId) => [
                { type: 'BookReaders', id: bookId },
                ...users.map(({ id }) => ({ type: 'User' as const, id }))
            ]
        }),
        addUser: build.mutation<User, UserCreate>({
            query: user => ({
                url: BASE_URI,
                method: 'POST',
                body: user
            }),
            invalidatesTags: (_result, error) => ignoreOnError(error, 'User')
        }),
        updateUser: build.mutation<User, { id: number, user: UserUpdate }>({
            query: ({ user, id }) => ({
                url: `${BASE_URI}/${id}`,
                method: 'PUT',
                body: user
            }),
            invalidatesTags: (_result, error, { id }) => ignoreOnError(error, { type: 'User', id })
        }),
        deleteUser: build.mutation<void, number>({
            query: id => ({
                url: `${BASE_URI}/${id}`,
                method: 'DELETE'
            }),
            invalidatesTags: (_result, error, id) => ignoreOnError(error, { type: 'User', id }, 'Borrow')
        })
    }),
    overrideExisting: false
});

export const {
    useGetUsersQuery,
    useAddUserMutation,
    useUpdateUserMutation,
    useDeleteUserMutation,
    useGetUsersByBookQuery
} = userApi;