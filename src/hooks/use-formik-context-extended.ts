import { useFormikContext } from "formik";
import { useContext } from "react";
import { FormikExtendedContext } from "../contexts/formik-extended.context";
import { FormikExtendedContextType } from "../types/formik-extended/formik-extended-context-type";

export function useFormikContextExtended<Values>(): FormikExtendedContextType<Values> {

    const formik = useFormikContext<Values>();

    const disabled = useContext(FormikExtendedContext);

    return ({ ...formik, disabled })
}