import { useContext } from "react";
import { DialogContext } from "../contexts/dialog.context";
import { DialogContextType } from "../types/dialog/dialog-context.type";

export const useDialog = (): DialogContextType => {
    const context = useContext(DialogContext);

    if (!context) {
        throw new Error('useDialog must be used within a Provider')
    }

    return context;
}